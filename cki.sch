<?xml version='1.0' encoding='utf-8'?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron">
  <pattern>
    <!-- Check CKI_IS_TEST is either "False" or "True" -->
    <rule context="param[@name='CKI_IS_TEST']/@value">
      <assert test=". = 'false' or . = 'true' or . = 'False' or . = 'True'">
        CKI_IS_TEST param of task "<value-of select="../../../@name"/>"
        is not an accepted boolean string: "<value-of select="."/>".
      </assert>
    </rule>
  </pattern>
  <pattern>
    <!-- Check each task has exactly one CKI_IS_TEST param -->
    <rule context="task">
      <assert test="count(params/param[@name='CKI_IS_TEST']) = 1">
        Task "<value-of select="@name"/>" doesn't have a single CKI_IS_TEST param.
      </assert>
    </rule>
  </pattern>
</schema>
